Clone the repository:

git clone git@gitlab.com:schulcza/weather-app.git

----------
Switch to the repo folder

cd weather-app

----------
Install all the dependencies using composer

composer install

----------
Copy the example env file and make the required configuration changes in the .env file

cp .env.example .env

----------
Generate a new application key

php artisan key:generate

----------
Start the local development server

php artisan serve

----------
Run the console script:

php artisan weather:get

