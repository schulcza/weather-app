<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Weather extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weather:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Időjárási adatok lekérése város alapján';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->results = [];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $city = $this->ask('Melyik város adatai érdekelnek? (Budapest)');

        // Set default city
        if ($city == '') {
            $city = 'Budapest';
        }

        $client = new \Goutte\Client();

        $weatherData    = $client->request('GET', 'https://www.idokep.hu/idojaras/' . $city);
        $row            = $weatherData->filter('.oszlop');

        $row->filter('.max-homerseklet-default')->each(function($currentMax, $key) {

            if ($key < 7) {
                $currDay = 'day' . $key;
                $this->results[$currDay] = [];

                $arr = explode("\n",trim($currentMax->text()));

                $this->results[$currDay]['max'] = $arr[0];
            }

        });

        $row->filter('.min-homerseklet-default')->each(function($currentMin, $key){

            if ($key < 7) {
                $arr = explode("\n",trim($currentMin->text()));
                
                $currDay = 'day' . $key;

                $this->results[$currDay]['min'] = $arr[0];
            }
        });

        // Calculate temperature fluctuation
        foreach ($this->results as $key => $item) {
            $this->results[$key]['fluctuation'] = $item['max'] - $item['min'];
        }

        // Calculate min
        $max = 0;
        
        foreach($this->results as $point){
            if($max < (float)$point['fluctuation']){
                $max = $point['fluctuation'];
            }
        }

        // Calculate max
        $min = $this->results['day0']['fluctuation'];

        foreach($this->results as $point){
            if($min > (float)$point['fluctuation']){
                $min = $point['fluctuation'];
            }
        }

        // Calculate average
        $sum = 0;
        foreach ($this->results as $key => $item) {
            $sum += $item['fluctuation'];
        }
        $average =  $sum / count($this->results);

        // Calculate median
        foreach ($this->results as $key => $item) {
           $numbers[] = $item['fluctuation'];
        }

        sort($numbers);

        $count = count($numbers);
        $index = floor($count/2);

        if (!$count) {
            $this->error('Nincsenek adatok');
        } elseif ($count & 1) {
            $median = $numbers[$index];
        } else {
            $median = ($numbers[$index-1] + $numbers[$index]) / 2;
        }

        // Calculate scatter
        foreach ($this->results as $key => $result) {
            $this->results[$key]['diff'] = $result['fluctuation'] - $average;
            $this->results[$key]['diffSquare'] = pow(($result['fluctuation'] - $average), 2);
        }

        $sumPow = 0;

        foreach ($this->results as $key => $result) {
            $sumPow += $this->results[$key]['diffSquare'];
        }

        $x = $sumPow / count($this->results);

        $scatter = sqrt($x);

        // Cereate table output
        $headers = ['Minimum hőingadozás', 'Maximum hőingadozás', 'Átlag', 'Medián', 'Szórás'];

        $return = [
            [
                'min' => $min,
                'max' => $max,
                'average' => $average,
                'median' => $median,
                'scatter' => $scatter,
            ]
        ];

        $this->line($city . ' következő 7 napjának eredményei:');
        $this->table($headers, $return);

    }
}
